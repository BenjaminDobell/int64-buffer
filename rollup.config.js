export default {
    entry: 'int64-buffer.js',
    targets: [
        {
            dest: 'dist/int64-buffer.js',
            format: 'iife',
            moduleName: 'int64buffer',
            sourceMap: true
        }
    ]
}
